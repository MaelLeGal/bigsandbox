// Copyright Epic Games, Inc. All Rights Reserved.

#include "BigSandBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BigSandBox, "BigSandBox" );
